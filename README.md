# Rakning C-19 - iOS

This project is intended to share and ensure that the official Rakning-C19 for Iceland implementing the exposure notification API developed by Apple and Google is requesting any additional data or doing anything else other than detect and notify users about possible exposures and negative Covid-19 test result made by the Icelandic Ministry of Health.

# The highlevel process

This app does the following:

* Pick up exposure keys emitted from nearby devices and store
* Emit exposure keys 
* Share emitted keys to a central cerver when a user has been diagnosed with covid-19
* Download 'infected keys' from the central key server
* Iterated over downloaded keys and check if a matching key has been picked up
* Estimate risk and alert user

## Secondary features

* Register phone number to reveice push on covid test result (Currently only for negative tests results from tests performed up on entering Iceland via Keflavik airport)
* Inform users about covid
* Guide users that have been identified by the app as having possibly been exposed to a person which has shared keys after receiving a positive covid test

## Services

Key and verification servers are hosted at origo. They are also based on the example design by google. 

System to alert users receiving negative tests from taking a covid test when entering Iceland via Keflavik areport is hosted and stored at Aranja

# Internal DEV guidelines
The app is based on googles express version of the GAEN system. The idea is that you can generate a styled and customesed app using a config JSON. Therefore the gradle process will generate build flavors with config and strings derived from the config jsons located in the app/config folder. The configs are located in confluence


The git strategy is github-flow. Branch from master, kepp changes small and short lived. 
Use tags to version the app. Merge changes to master and tag those versions uploaded to playstore. 


The rest of this document is carry over from googles original app:

# Exposure Notifications API: Android Reference Design

This is a reference design for an Android app implementing the
[Exposure Notifications API](https://www.blog.google/inside-google/company-announcements/apple-and-google-partner-covid-19-contact-tracing-technology/)
provided by Apple and Google.

Only approved government public health authorities can access the APIs
(included in an upcoming build of `Google Play services`) to build an app for
COVID-19 response efforts. As part of our continued efforts to be transparent,
we’re making this code public to help approved government public health
authorities understand how to use the `Exposure Notifications API` to build
their COVID-19 apps.

Read the [Exposure Notifications API Terms of Service](https://google.com/covid19/exposurenotifications)

## Rakning C-19 Features

The application has tree main features which are a negative Covid-19 test result, the ability to share random generated keys once a person has been diagnosed with Covid-19 and the detection and notification of possible Covid-19 exposures. This features are described below.

### Negative Covid-19 Test Result Notification

This feature enables the application to receive the negative test result made by the Icelandic Minister of Health. In order to enable this feature the user must register their phone numbers in the application, after this the user will receive notifications of the following negative test result that the Minister makes to the citizen, please notice that if the result is positive the process to inform the citizen will be different. The phone validation server can be found [here](repo_to_validation_server).

### Share Phone Random Keys When Diagnosed with Covid-19

In order to share the random generated keys the citizen must receive a valid positive test result from the Icelandic Minister of Health along with a verification code which will be needed by the application in order to upload the random generated keys. Also the citizen permission is needed to upload the keys. For more information regarding the way that the exposure notification framework work press [here](https://www.google.com/covid19/exposurenotifications/). The upload server implementation can be found [here]().

### Covid-19 Possible Exposure Detection and Notification

The application checks at least once a day for possible exposures to anyone that has been diagnosis with Covid-19. This is done using the Exposure Notification Framework which exchange the random generated keys with other phones and downloads the exposure keys uploaded once a citizen has been diagnosis and decided to share the keys generated by the phone. Once the Exposure Notification Framework has detected a possible exposure the application will show a notification to the user of this and in the application the citizen can get further instructions.

## Getting Started

This project uses the Gradle build system. To build this code, use the
`gradlew build` command or use `Import Project` in Android Studio.

## Links

- [Overview of COVID-19 Exposure Notifications](https://www.blog.google/documents/66/Overview_of_COVID-19_Contact_Tracing_Using_BLE_1.pdf)
- [Frequently Asked Questions](https://g.co/ExposureNotificationFAQ)
- [Exposure Notification Bluetooth Specification](https://www.blog.google/documents/70/Exposure_Notification_-_Bluetooth_Specification_v1.2.2.pdf)
- [Exposure Notification Cryptography Specification](https://blog.google/documents/69/Exposure_Notification_-_Cryptography_Specification_v1.2.1.pdf)
- [Android Exposure Notifications API](https://developers.google.com/android/exposure-notifications/exposure-notifications-api)
- [Exposure Key Export and File Format](https://developers.google.com/android/exposure-notifications/exposure-key-file-format)
