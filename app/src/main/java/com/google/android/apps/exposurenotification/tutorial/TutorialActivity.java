/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.tutorial;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.transition.TransitionInflater;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.apps.exposurenotification.R;
import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class TutorialActivity extends AppCompatActivity {


  private static final String CURRENT_TUTORIAL_SCREEN = "Tutorial.Activity.CurrentTutorialScreen";


  Fragment[] fragments = {
      TutorialFragment.newInstance(R.layout.fragment_tutorial_info_screen_1),
      TutorialFragment.newInstance(R.layout.fragment_tutorial_info_screen_2),
      TutorialFragment.newInstance(R.layout.fragment_tutorial_info_screen_3),
      TutorialFragment.newInstance(R.layout.fragment_tutorial_info_screen_4),
      TutorialFragment.newInstance(R.layout.fragment_tutorial_info_screen_5)
  };
  int currentFragment = 0;
  private TransitionInflater inflater;
  private TutorialViewModel tutorialViewModel;


  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_tutorial);
    this.findViewById(R.id.tutorial_next_button).setOnClickListener(v -> next());
    inflater = TransitionInflater.from(this);
    tutorialViewModel = new ViewModelProvider(this).get(TutorialViewModel.class);
  }


  @Override
  protected void onResume() {
    super.onResume();
    getSupportFragmentManager()
        .beginTransaction()
        .addToBackStack(null)
        .replace(R.id.tutorial_fragment, fragments[currentFragment])
        .commit();
  }

  @Override
  protected void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putInt(CURRENT_TUTORIAL_SCREEN, currentFragment);
  }

  @Override
  protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    if (savedInstanceState.containsKey(CURRENT_TUTORIAL_SCREEN)) {
      currentFragment = savedInstanceState.getInt(CURRENT_TUTORIAL_SCREEN);
    }
  }

  @Override
  public void onBackPressed() {
    if (currentFragment > 0) {
      TransitionInflater inflater = TransitionInflater.from(this);
      fragments[currentFragment]
          .setExitTransition(inflater.inflateTransition(R.transition.slide_right));
      currentFragment--;
      fragments[currentFragment]
          .setEnterTransition(inflater.inflateTransition(R.transition.slide_left));
      fragments[currentFragment]
          .setExitTransition(inflater.inflateTransition(R.transition.slide_left));
      switchFragment();
    }

  }


  public static Intent newIntentForTutorialScreens(Context context) {
    return new Intent(context, TutorialActivity.class);
  }

  void next() {
    currentFragment++;
    if (currentFragment >= fragments.length) {
      this.tutorialViewModel.setTutorialAllDone(true);
      finish();
      return;
    }
    fragments[currentFragment]
        .setEnterTransition(inflater.inflateTransition(R.transition.slide_right));
    fragments[currentFragment]
        .setExitTransition(inflater.inflateTransition(R.transition.slide_left));
    switchFragment();
  }

  private void switchFragment() {
    getSupportFragmentManager()
        .beginTransaction()
        .addToBackStack(null)
        .replace(R.id.tutorial_fragment, fragments[currentFragment])
        .commit();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    this.fragments = null;
    this.inflater = null;
  }
}