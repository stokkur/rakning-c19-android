/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.push;

import android.util.Log;
import androidx.annotation.NonNull;
import com.google.android.apps.exposurenotification.network.AranjaPushServiceController;
import com.google.android.apps.exposurenotification.network.AranjaUser;
import com.google.android.apps.exposurenotification.storage.ExposureNotificationSharedPreferences;
import com.google.common.base.Strings;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import dagger.hilt.android.AndroidEntryPoint;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import javax.inject.Inject;



@AndroidEntryPoint
public class ForegroundMessageHandler extends FirebaseMessagingService {

  public static final String ON_PUSH_RECEIVED = "ForegroundMessageHandler.ON_PUSH_RECEIEVED";
  public static final String TEST_RESULTS = "ForegroundMessageHandler.TEST_RESULTS";
  private final String TAG = ForegroundMessageHandler.class.getSimpleName();

  @Inject
  AranjaPushServiceController aranjaPushServiceController;

  @Inject
  ExposureNotificationSharedPreferences exposureNotificationSharedPreferences;


  @Override
  public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
    super.onMessageReceived(remoteMessage);
    String aranjaToken = exposureNotificationSharedPreferences.loadAranjaToken();
    if (!Strings.isNullOrEmpty(aranjaToken)) {
      try {
        AranjaUser aranjaUser = aranjaPushServiceController.loadUser(aranjaToken).get();
        if (aranjaUser != null && aranjaUser.getTestResult() != null) {
          startActivity(DisplayTestResultsDialogActivity.newIntent(aranjaUser, getApplicationContext()));
        }
      } catch (ExecutionException | InterruptedException ignored) { }
    }
  }

  @Override
  public void onNewToken(String token) {
    Log.d(TAG, "Refreshed token: " + token);
    try {
      String aranjaToken = exposureNotificationSharedPreferences.loadAranjaToken();
      if (!Strings.isNullOrEmpty(aranjaToken)) {
        aranjaPushServiceController.putFirebaseToken(token, aranjaToken);
        Log.i(TAG, "Stored new firebasetoken");
      }
    } catch (Exception e) {
      Log.e(TAG, Objects.requireNonNull(e.getMessage()));
    }
  }
}
