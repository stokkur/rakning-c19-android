/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.push;


import static com.google.android.apps.exposurenotification.settings.push.PhoneRegistrationActivity.PHONE_NUMBER;
import static com.google.android.apps.exposurenotification.settings.push.PhoneRegistrationActivity.PHONE_REGISTRATION_VERIFICATION_TOKEN;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.apps.exposurenotification.R;
import com.google.android.apps.exposurenotification.common.AbstractTextWatcher;
import com.google.android.material.snackbar.Snackbar;
import dagger.hilt.android.AndroidEntryPoint;
import java.util.Locale;

@AndroidEntryPoint
public class PhoneRegistrationVerificationFragment extends Fragment {


  private PhoneRegistrationViewModel phoneRegistrationViewModel;
  private String token;
  private String phoneNumber;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    token = getArguments().getString(PHONE_REGISTRATION_VERIFICATION_TOKEN);
    phoneNumber = getArguments().getString(PHONE_NUMBER);
    return inflater
        .inflate(R.layout.fragment_phone_registration_enter_verification_code, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    phoneRegistrationViewModel = new ViewModelProvider(getActivity())
        .get(PhoneRegistrationViewModel.class);

    EditText input = view.findViewById(R.id.register_activation_code_input);
    Button submitBtn = view.findViewById(R.id.register_phone_btn);

    input.addTextChangedListener(new AbstractTextWatcher() {
      @Override
      public void afterTextChanged(Editable s) {
        submitBtn.setEnabled(!TextUtils.isEmpty(input.getText()));
      }
    });

    view.findViewById(R.id.send_again_link)
        .setOnClickListener(
            (v) -> phoneRegistrationViewModel.registerNumber(phoneNumber));

    view.findViewById(R.id.previous_button)
        .setOnClickListener((v) -> getActivity().onBackPressed());


    submitBtn.setContentDescription("Register phone");

    Locale locale = getResources().getConfiguration().locale;

    submitBtn.setOnClickListener(
        (v) -> phoneRegistrationViewModel.submitPin(token, input.getText().toString(), locale.getLanguage()));

    submitBtn.setEnabled(!TextUtils.isEmpty(input.getText()));

    phoneRegistrationViewModel.subscribeErrorEvent()
        .observe(getViewLifecycleOwner(), this::maybeShowSnackbar);
  }

  private void maybeShowSnackbar(Integer stringReourceId) {
    View view = getView();
    if (view != null) {
      Snackbar.make(view.findViewById(R.id.snackbar_target), getResources().getString(stringReourceId), Snackbar.LENGTH_SHORT)
          .show();
    }
  }


}



