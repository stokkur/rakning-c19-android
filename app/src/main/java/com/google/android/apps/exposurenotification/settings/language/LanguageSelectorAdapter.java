/*
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.language;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.google.android.apps.exposurenotification.R;
import com.google.android.apps.exposurenotification.utils.CountryFlags;
import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

class LanguageSelectorAdapter extends BaseAdapter {


  private final LayoutInflater inflater;
  private final ArrayList<LanguageSelectionItem> items;
  private List<Locale> allLocales;

  public LanguageSelectorAdapter(@NonNull Context context) {
    inflater = (LayoutInflater.from(context));
    items = new ArrayList<>();

    allLocales = Arrays.asList(Locale.getAvailableLocales());

    for (String countryCode : countryCodes) {
      LanguageSelectionItem languageSelectionItem = new LanguageSelectionItem();
      String[] splitTag = countryCode.split("-");
      Locale l;
      String localeCountryCode;
      if (splitTag.length > 1) {
        l = new Locale(splitTag[0], splitTag[splitTag.length - 1]);
        localeCountryCode = splitTag[splitTag.length - 1];
      } else {
        if (isISOCompatible(countryCode)) {
          localeCountryCode = countryCode.toUpperCase();
          l = new Locale(countryCode, localeCountryCode);
        } else {
          String candidateCode = locateFlagISOFromLocales(countryCode);
          if (Strings.isNullOrEmpty(candidateCode)) {
            l = Locale.forLanguageTag(countryCode);
            localeCountryCode = l.toLanguageTag().toUpperCase();
          } else {
            l = new Locale(countryCode, candidateCode);
            localeCountryCode = candidateCode;
          }
        }
      }

      languageSelectionItem.locale = l;
      languageSelectionItem.flagId = CountryFlags.getCountryFlagByCountryCode(localeCountryCode);

      items.add(languageSelectionItem);
    }

    if (VERSION.SDK_INT >= VERSION_CODES.N) {
      items.sort((o1, o2) -> o1.locale.getDisplayLanguage().compareTo(o2.locale.getDisplayLanguage()));
    }

  }

  private boolean isISOCompatible(String countryCode) {
    for (String l : Locale.getISOCountries()) {
      if (l.toLowerCase().equals(countryCode.toLowerCase())) {
        return true;
      }
    }
    return false;
  }

  private String locateFlagISOFromLocales(String countryCode) {
    for (Locale candidate : allLocales) {
      String[] splitTag = candidate.toLanguageTag().split("-");
      if (splitTag[0].equals(countryCode)) {
        if (splitTag.length == 2) {
          return splitTag[1];
        }
      }
    }
    return null;
  }

  @Override
  public int getCount() {
    return items.size();
  }

  @Override
  public LanguageSelectionItem getItem(int position) {
    return items.get(position);
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }


  @Override
  public View getDropDownView(int i, View view, ViewGroup parent) {
    view = inflater.inflate(R.layout.onboarding_languagespinner_item, null);
    TextView icon = view.findViewById(R.id.spinner_item_flag);

    icon.setText(items.get(i).flagId);

    TextView countryName = view.findViewById(R.id.spinner_item_language);
    String languageName = items.get(i).locale.getDisplayLanguage();
    String displayFormat = "";

    displayFormat = languageName.substring(0, 1).toUpperCase() + languageName
        .substring(1);
    countryName.setText(displayFormat + " - " + items.get(i).locale.getDisplayCountry());

    return view;
  }

  @Override
  public View getView(int i, View view, ViewGroup parent) {
    return getDropDownView(i, view, parent);
  }

  public int itemFromLocale(Locale locale) {
    for (int i = 0; i < items.size(); i++) {
      if (items.get(i).locale.getDisplayLanguage().equals(locale.getDisplayLanguage())) {
        return i;
      }
    }
    return 0;
  }

  class LanguageSelectionItem {

    private String flagId;
    private Locale locale;


    public Locale getLocale() {
      return locale;
    }
  }

  private List<String> countryCodes = Arrays.asList(
      "af-NA",
      "am",
      "ar-EG",
      "ar-JO",
      "ar-MA",
      "ar-SA",
      "as-IN",
      "az-AZ",
      "bg",
      "bs-BA",
      "ca-ES",
      "cs",
      "da",
      "de",
      "de-AT",
      "de-CH",
      "el-GR",
      "en-AU",
      "en-CA",
      "en-GB",
      "en-IE",
      "en-NZ",
      "en-ZA",
      "es-ES",
      "es-AR",
      "es-BO",
      "es-CL",
      "es-CO",
      "es-CR",
      "es-DO",
      "es-EC",
      "es-GT",
      "es-HN",
      "es-MX",
      "es-NI",
      "es-PA",
      "es-PE",
      "es-PR",
      "es-PY",
      "es-SV",
      "es-UY",
      "es-VE",
      "et-EE",
      "eu",
      "fa-IR",
      "fi",
      "fr-BE",
      "fr-CA",
      "fr-CH",
      "gl-ES",
      "gu-IN",
      "he",
      "hi",
      "hr",
      "hu",
      "hy",
      "id",
      "is",
      "it",
      "ja",
      "ka",
      "kk",
      "kn-IN",
      "ko-KR",
      "ky-KG",
      "lt",
      "lv",
      "mk",
      "mn",
      "mr",
      "ms-MY",
      "nb",
      "nl",
      "nn-NO",
      "pa-IN",
      "pl",
      "pt",
      "pt-BR",
      "pt-PT",
      "ro",
      "ru",
      "sk",
      "sl-SI",
      "sq",
      "sr-Cyrl-BA",
      "sv-SE",
      "sw",
      "ta-LK",
      "te",
      "th",
      "tl-PH",
      "tr",
      "uk",
      "ur-PK",
      "uz-UZ",
      "vi-VN",
      "zh-CN",
      "zh-HK",
      "zh-TW",
      "zu"
  );

}
