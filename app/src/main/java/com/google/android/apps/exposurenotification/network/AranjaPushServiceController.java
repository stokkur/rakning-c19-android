/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.network;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.concurrent.futures.CallbackToFutureAdapter.Completer;
import androidx.concurrent.futures.CallbackToFutureAdapter.Resolver;
import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.apps.exposurenotification.R;
import com.google.common.util.concurrent.ListenableFuture;
import dagger.hilt.android.qualifiers.ApplicationContext;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;

public class AranjaPushServiceController {

  private static final String TAG = AranjaPushServiceController.class.getSimpleName();
  private final String URL;

  private final RequestQueueWrapper queue;

  @Inject
  AranjaPushServiceController(RequestQueueWrapper queue, @ApplicationContext Context context) {
    this.queue = queue;
    this.URL = context.getResources().getString(R.string.enx_aranjaUrl);
  }


  public ListenableFuture<String> requestPin(String number) {
    JSONObject postData = new JSONObject();
    try {
      postData.put("phone", number);
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return CallbackToFutureAdapter.getFuture(new AranjaRequestWrapper<>(Method.POST,
        URL + "user/requestpin", postData, queue, response -> {
      try {
        return response.getString("token");
      } catch (JSONException e) {
        throw new RuntimeException("Failed at parsing response " + e.getMessage());
      }
    }, null));

  }

  private ErrorListener loadErrorListener(Completer completer) {
    return
        err -> completer.setException(new Exception(VolleyUtils.getErrorMessage(err)));

  }

  public ListenableFuture<String> validatePin(String phone, String pin, String token,
      String locale) {
    JSONObject postData = new JSONObject();
    try {
      postData.put("phone", phone);
      postData.put("pin", pin);
      postData.put("token", token);
      postData.put("locale", locale);
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return CallbackToFutureAdapter.getFuture(
        new AranjaRequestWrapper<>(Method.POST, URL + "user/pin", postData, queue,
            response -> {
              try {
                return response.getString("token");
              } catch (JSONException e) {
                throw new RuntimeException("Failed at parsing response " + e.getMessage());
              }
            }, null));
  }


  public ListenableFuture putFirebaseToken(String firebaseToken, String aranjaToken) {
    JSONObject postData = new JSONObject();
    try {
      postData.put("pushToken", firebaseToken);
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return CallbackToFutureAdapter.getFuture(completer -> {
      Listener<String> responseListener =
          completer::set;

      StringRequest stringRequest = new StringRequest(Method.PUT, URL + "user", responseListener,
          loadErrorListener(completer)) {


        @Override
        public String getBodyContentType() {
          return "application/json; charset=utf-8";
        }


        @Override
        public byte[] getBody() {
          try {
            return postData.toString().getBytes("utf-8");
          } catch (UnsupportedEncodingException e) {
            return null;
          }
        }

        @Override
        public Map<String, String> getHeaders() {
          Map<String, String> headers = new HashMap<>();
          headers.put("Authorization", "Bearer " + aranjaToken);
          return headers;
        }
      };

      // Add the request to the RequestQueue.
      queue.add(stringRequest);
      return stringRequest;
    });
  }

  public ListenableFuture<AranjaUser> loadUser(String aranjaToken) {

    return CallbackToFutureAdapter
        .getFuture(new AranjaRequestWrapper<>(Method.GET, URL + "user/", null, queue,
            response -> {

              try {
                AranjaUser user = new AranjaUser();
                user.setDataRequested(
                    response.has("dataRequested") && response.getBoolean("dataRequested"));
                user.setLocale(
                    response.has("locale") ? response.getString("locale") : "");
                user.setRequiresKennitala(
                    response.has("requiresKennitala") && response.getBoolean("requiresKennitala"));

                Boolean testResults = null;
                if (response.has("testResult")) {
                  if (!response.isNull("testResult")) {
                    testResults = response.getBoolean("testResult");
                  }
                }

                user.setTestResult(testResults);
                return user;
              } catch (JSONException e) {
                throw new RuntimeException("Failed at parsing response " + e.getMessage());
              }
            }, () -> getAuthHeaders(aranjaToken)));
  }


  private Map<String, String> getAuthHeaders(String aranjaToken) {
    Map<String, String> headers = new HashMap<>();
    headers.put("Authorization", "Bearer " + aranjaToken);
    return headers;
  }


  public ListenableFuture deleteTestResults(String aranjaToken) {
    return CallbackToFutureAdapter.getFuture(completer -> {
      Listener<String> responseListener =
          completer::set;

      StringRequest stringRequest = new StringRequest(Method.DELETE, URL + "user/test-result",
          responseListener,
          loadErrorListener(completer)) {


        @Override
        public Map<String, String> getHeaders() {
          Map<String, String> headers = new HashMap<>();
          headers.put("Authorization", "Bearer " + aranjaToken);
          return headers;
        }
      };

      // Add the request to the RequestQueue.
      queue.add(stringRequest);
      return stringRequest;
    });
  }
}


class AranjaRequestWrapper<T> implements Resolver<T> {

  private final int method;
  private final String url;
  private final JSONObject requestData;
  private final RequestQueueWrapper queue;
  private final ResponseParser<T> responseParser;
  private final AuthInterceptor authInterceptor;

  @Nullable
  @Override
  public Object attachCompleter(@NonNull Completer<T> completer) {
    JsonObjectRequest request = new JsonObjectRequest(method, url, requestData,
        response -> completer.set(responseParser.parseResponse(response)),
        error -> completer.setException(new Exception(VolleyUtils.getErrorMessage(error)))) {
      @Override
      public Map<String, String> getHeaders() throws AuthFailureError {
        return authInterceptor == null ? super.getHeaders() : authInterceptor.getHeaders();
      }
    };
    queue.add(request);
    return request;
  }

  public AranjaRequestWrapper(int method, String url, JSONObject requestData,
      RequestQueueWrapper queue, ResponseParser<T> responseParser,
      @Nullable AuthInterceptor authInterceptor) {
    this.method = method;
    this.url = url;
    this.requestData = requestData;
    this.queue = queue;
    this.responseParser = responseParser;
    this.authInterceptor = authInterceptor;
  }

}

interface ResponseParser<T> {

  T parseResponse(JSONObject response);
}

interface AuthInterceptor {

  Map<String, String> getHeaders();
}