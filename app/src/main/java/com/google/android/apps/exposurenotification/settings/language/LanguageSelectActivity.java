/*
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.language;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.apps.exposurenotification.R;
import dagger.hilt.android.AndroidEntryPoint;
import java.util.Locale;


@AndroidEntryPoint
public class LanguageSelectActivity extends AppCompatActivity implements OnItemSelectedListener {

  public static final int NEW_LANGUAGE_SELECTED = "NEW_LANGUAGE_SELECTED".hashCode();
  private LanguageSelectViewModel languageSelectViewModel;
  private LanguageSelectorAdapter adapter;
  private Locale selectedLocale;


  public static Intent newIntent(Context context) {
    return new Intent(context, LanguageSelectActivity.class);
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    languageSelectViewModel = new ViewModelProvider(this).get(LanguageSelectViewModel.class);

    setContentView(R.layout.activity_language_select);
    setupSpinner(findViewById(R.id.country_language_spinner));
    findViewById(R.id.cancel).setOnClickListener(v -> finish());
    findViewById(R.id.confirm).setOnClickListener(v -> saveLanguageAndFinish());
  }

  private void saveLanguageAndFinish() {
    getResources().getConfiguration().setLocale(selectedLocale);
    Resources res = getResources();
    DisplayMetrics dm = res.getDisplayMetrics();
    Configuration conf = res.getConfiguration();
    conf.locale = selectedLocale;
    languageSelectViewModel.saveLanguage(selectedLocale.toLanguageTag());
    res.updateConfiguration(conf, dm);
    setResult(NEW_LANGUAGE_SELECTED);
    finish();
  }


  private void setupSpinner(Spinner spinner) {
    adapter = new LanguageSelectorAdapter(this);
    spinner.setAdapter(adapter);
    spinner.setSelection(adapter.itemFromLocale(getResources().getConfiguration().locale));
    spinner.setOnItemSelectedListener(this);
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    selectedLocale = adapter.getItem(position).getLocale();
  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {

  }
}

