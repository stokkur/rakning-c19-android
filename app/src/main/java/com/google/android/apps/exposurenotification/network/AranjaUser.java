/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.network;

public class AranjaUser {

  private String locale;
  private boolean dataRequested;
  private boolean requiresKennitala;
  private Boolean testResult;

  public String getLocale() {
    return locale;
  }

  public void setLocale(String locale) {
    this.locale = locale;
  }

  public boolean isDataRequested() {
    return dataRequested;
  }

  public void setDataRequested(boolean dataRequested) {
    this.dataRequested = dataRequested;
  }

  public boolean isRequiresKennitala() {
    return requiresKennitala;
  }

  public void setRequiresKennitala(boolean requiresKennitala) {
    this.requiresKennitala = requiresKennitala;
  }

  public Boolean getTestResult() {
    return testResult;
  }

  public void setTestResult(Boolean testResult) {
    this.testResult = testResult;
  }
}
