/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.push;

import android.util.Log;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.ViewModel;
import androidx.work.ListenableWorker.Result;
import com.blongho.country_data.Country;
import com.blongho.country_data.World;
import com.google.android.apps.exposurenotification.R;
import com.google.android.apps.exposurenotification.common.Qualifiers.BackgroundExecutor;
import com.google.android.apps.exposurenotification.common.Qualifiers.LightweightExecutor;
import com.google.android.apps.exposurenotification.common.SingleLiveEvent;
import com.google.android.apps.exposurenotification.network.AranjaPushServiceController;
import com.google.android.apps.exposurenotification.network.Connectivity;
import com.google.android.apps.exposurenotification.settings.push.PhoneNumberDropDownAdapter.PhonePickerItem;
import com.google.android.apps.exposurenotification.storage.ExposureNotificationSharedPreferences;
import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.Futures;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;

public class PhoneRegistrationViewModel extends ViewModel {

  private static final String TAG = PhoneRegistrationViewModel.class.getSimpleName();
  private final ExposureNotificationSharedPreferences exposureNotificationSharedPreferences;
  private final AranjaPushServiceController aranjaPushServiceController;
  private final ExecutorService backgroundExecutor;
  private final ExecutorService lightweightExecutor;
  private final Connectivity connectivity;
  private SingleLiveEvent<Integer> errorStringResourceIdEvent = new SingleLiveEvent<>(); // To fetch string resources
  private SingleLiveEvent<String> verificationToken = new SingleLiveEvent<>();
  private SingleLiveEvent<List<PhonePickerItem>> codes = new SingleLiveEvent<>();
  private int telephoneCode;


  private String phoneNumber;
  private SingleLiveEvent<Boolean> tokenStored = new SingleLiveEvent<>();
  private SingleLiveEvent<Boolean> registrationComplete = new SingleLiveEvent<>();
  private int defaultPickerItem;
  private List<PhonePickerItem> codesList;

  @ViewModelInject
  public PhoneRegistrationViewModel(
      ExposureNotificationSharedPreferences exposureNotificationSharedPreferences,
      AranjaPushServiceController aranjaPushServiceController,
      @BackgroundExecutor ExecutorService backgroundExecutor,
      @LightweightExecutor ExecutorService lightweightExecutor,
      Connectivity connectivity
  ) {
    this.exposureNotificationSharedPreferences = exposureNotificationSharedPreferences;
    this.aranjaPushServiceController = aranjaPushServiceController;
    this.backgroundExecutor = backgroundExecutor;
    this.lightweightExecutor = lightweightExecutor;
    this.connectivity = connectivity;
  }

  void registerNumber(String number) {

    Integer error = validate(number);
    if (error != null) {
      errorStringResourceIdEvent.postValue(error);
      return;
    }

    phoneNumber = "+" + telephoneCode + number;
    this.exposureNotificationSharedPreferences.storePhoneNumber(phoneNumber);

    FluentFuture.from(aranjaPushServiceController.requestPin(phoneNumber))
        .transformAsync(token -> {
          verificationToken.postValue(token);
          return Futures.immediateFuture(Result.success());
        }, backgroundExecutor)
        .catching(
            Exception.class,
            ex -> {
              errorStringResourceIdEvent.postValue(R.string.phone_registration_generic_error);
              return null;
            },
            lightweightExecutor);
  }

  public void submitPin(String token, String verificationCode, String locale) {
    FluentFuture
        .from(aranjaPushServiceController.validatePin(getPhoneNumber(), verificationCode, token, locale))
        .transformAsync(login -> {
          storeUserToken(login);
          return Futures.immediateFuture(Result.success());
        }, backgroundExecutor)
        .catching(
            Exception.class,
            ex -> {
              errorStringResourceIdEvent.postValue(R.string.phone_registration_generic_error);
              return null;
            },
            lightweightExecutor);
  }

  public void registerFirebaseToken() {

    FirebaseMessaging.getInstance().getToken()
        .addOnCompleteListener(task -> {
          if (!task.isSuccessful()) {
            return;
          }
          String firebaseToken = task.getResult();
          Log.d(TAG, "Obtained firebaseToken: " + firebaseToken);
          FluentFuture
              .from(aranjaPushServiceController.putFirebaseToken(firebaseToken,
                  exposureNotificationSharedPreferences.loadAranjaToken()))
              .transformAsync(login -> {
                registrationComplete.postValue(true);
                return Futures.immediateFuture(Result.success());
              }, backgroundExecutor)
              .catching(
                  Exception.class,
                  ex -> {
                    errorStringResourceIdEvent.postValue(R.string.phone_registration_generic_error);
                    return null;
                  },
                  lightweightExecutor);
        });
  }

  private void storeUserToken(String token) {
    exposureNotificationSharedPreferences.storeAranjaToken(token);
    tokenStored.postValue(true);
  }

  private Integer validate(String number) {
    if (number == null || number.isEmpty()) {
      return R.string.phone_number_registration_warning;
    }
    if (!connectivity.hasInternet()) {
      return R.string.phone_number_fragment_offline_warning;
    }
    return null;
  }

  public SingleLiveEvent<Integer> subscribeErrorEvent() {
    return errorStringResourceIdEvent;
  }

  public void loadCountryInfo() {
    backgroundExecutor.submit(() -> {
      if (codesList == null || codesList.isEmpty()) {
        codesList = new ArrayList<>();
        Set<String> regions = PhoneNumberUtil.getInstance().getSupportedRegions();
        Country country;
        for (String countryCode : regions) {
          country = World.getCountryFrom(countryCode);
          codesList.add(new PhonePickerItem(
              country.getName(),
              country.getFlagResource(),
              PhoneNumberUtil.getInstance().getCountryCodeForRegion(countryCode)
          ));
        }
        java.util.Collections.sort(codesList);
        findDefaultPickerItem(codesList);
      }
      codes.postValue(codesList);
    });
  }

  private void findDefaultPickerItem(List<PhonePickerItem> codes) {
    int i = 0;
    for (PhonePickerItem item : codes) {
      if (item.getTelephoneCode() == 354) {
        defaultPickerItem = i;
        break;
      }
      i++;
    }
  }

  public void setSelectedCountry(int telephoneCode) {
    this.telephoneCode = telephoneCode;
  }

  public SingleLiveEvent<String> subscribeVerificationToken() {
    return verificationToken;
  }

  public String getPhoneNumber() {
    return phoneNumber == null ? exposureNotificationSharedPreferences.loadPhoneNumber()
        : phoneNumber;
  }

  public SingleLiveEvent<Boolean> subscribeTokenStoredEvent() {
    return tokenStored;
  }

  public String loadToken() {
    return exposureNotificationSharedPreferences.loadAranjaToken();
  }

  public SingleLiveEvent<Boolean> getRegistrationComplete() {
    return registrationComplete;
  }

  public int getDefaultItem() {
    return defaultPickerItem;
  }

  public SingleLiveEvent<List<PhonePickerItem>> subscribeCountriesLoaded() {
    return codes;
  }

  public void setShouldPromptForPhoneNumber(boolean b) {
    exposureNotificationSharedPreferences.setShouldPromptForPhoneNumber(b);
  }

  public void deleteTestResults() {
    FluentFuture
        .from(aranjaPushServiceController
            .deleteTestResults(exposureNotificationSharedPreferences.loadAranjaToken()))
        .transformAsync(response -> Futures.immediateFuture(Result.success()), backgroundExecutor)
        .catching(
            Exception.class,
            ex -> null,
            lightweightExecutor);
  }

}
