/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.push;

import static com.google.android.apps.exposurenotification.settings.push.PhoneRegistrationActivity.TUTORIAL_FLOW;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.apps.exposurenotification.R;
import com.google.android.apps.exposurenotification.common.AbstractTextWatcher;
import com.google.android.material.snackbar.Snackbar;
import dagger.hilt.android.AndroidEntryPoint;


@AndroidEntryPoint
public class PhoneNumberFragment extends Fragment implements OnItemSelectedListener {


  private PhoneRegistrationViewModel phoneRegistrationViewModel;
  private EditText phoneInput;
  private Button submitBtn;
  private boolean isTutorialFlow;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    isTutorialFlow =
        getArguments() != null && getArguments().containsKey(TUTORIAL_FLOW) && getArguments()
            .getBoolean(TUTORIAL_FLOW);
    return inflater.inflate(R.layout.fragment_phone_registration_enter_number, container, false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    phoneRegistrationViewModel =
        new ViewModelProvider(getActivity()).get(PhoneRegistrationViewModel.class);

    // Navigate out
    View upButton = view.findViewById(android.R.id.home);
    upButton.setContentDescription(getString(R.string.navigate_up));
    upButton.setOnClickListener((v) -> getActivity().onBackPressed());
    Button previousButton = view.findViewById(R.id.previous_button);
    previousButton.setOnClickListener((v) -> getActivity().onBackPressed());

    View skip = view.findViewById(R.id.skip);
    skip.setOnClickListener(v -> {
          phoneRegistrationViewModel.setShouldPromptForPhoneNumber(false);
          getActivity().finish();
        }
    );
    skip.setVisibility(isTutorialFlow ? View.VISIBLE : View.GONE);

    previousButton.setVisibility(isTutorialFlow ? View.GONE : View.VISIBLE);

    phoneInput = view.findViewById(R.id.register_phone_number_input);
    submitBtn = view.findViewById(R.id.register_phone_btn);
    submitBtn.setContentDescription("Register phone");
    submitBtn.setOnClickListener((v) -> submitPhoneNumber());
    submitBtn.setEnabled(!TextUtils.isEmpty(phoneInput.getText()));

    phoneInput.addTextChangedListener(new AbstractTextWatcher() {
      @Override
      public void afterTextChanged(Editable s) {
        submitBtn.setEnabled(!TextUtils.isEmpty(phoneInput.getText()));
      }
    });

    final Spinner spinner = view.findViewById(R.id.country_phone_spinner);
    spinner.setOnItemSelectedListener(this);
    spinner.setEnabled(false);

    PhoneNumberDropDownAdapter customAdapter = new PhoneNumberDropDownAdapter(
        this.requireContext());

    phoneRegistrationViewModel.loadCountryInfo();

    phoneRegistrationViewModel.subscribeCountriesLoaded().observe(this,
        phonePickerItems -> {
          customAdapter.setItems(phonePickerItems);
          spinner.setAdapter(customAdapter);
          spinner.setSelection(phoneRegistrationViewModel.getDefaultItem());
          spinner.setEnabled(true);
          view.findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
        }
    );

    phoneRegistrationViewModel.subscribeErrorEvent()
        .observe(getViewLifecycleOwner(), this::maybeShowSnackbar);
  }


  private void maybeShowSnackbar(Integer stringResourceId) {
    View view = getView();
    if (view != null) {
      Snackbar
          .make(requireView(), getResources().getString(stringResourceId), Snackbar.LENGTH_SHORT)
          .show();
      submitBtn.setEnabled(true);
    }
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    PhoneNumberDropDownAdapter.PhonePickerItem selectedItem = (PhoneNumberDropDownAdapter.PhonePickerItem) parent
        .getItemAtPosition(position);
    phoneRegistrationViewModel.setSelectedCountry(selectedItem.getTelephoneCode());
  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {

  }

  private void submitPhoneNumber() {
    submitBtn.setEnabled(false);
    phoneRegistrationViewModel.registerNumber(String.valueOf(phoneInput.getText()));
  }


}
