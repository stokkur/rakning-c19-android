/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.push;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.google.android.apps.exposurenotification.R;
import java.util.ArrayList;
import java.util.List;

class PhoneNumberDropDownAdapter extends BaseAdapter {


  private final List<PhonePickerItem> items;
  private final LayoutInflater inflater;

  public PhoneNumberDropDownAdapter(@NonNull Context context) {
    inflater = (LayoutInflater.from(context));
    items = new ArrayList<>();
  }


  @Override
  public int getCount() {
    return items.size();
  }

  @Override
  public PhonePickerItem getItem(int position) {
    return items.get(position);
  }

  @Override
  public long getItemId(int position) {
    return 0;
  }

  @Override
  public View getDropDownView(int i, View view, ViewGroup parent) {
    view = inflater.inflate(R.layout.phonepicker_spinner_item, null);
    ImageView icon = (ImageView) view.findViewById(R.id.spinner_item_flag);

    PhonePickerItem item = items.get(i);
    icon.setImageResource(item.flagResourceId);

    TextView countryCode = (TextView) view.findViewById(R.id.spinner_item_country_code);
    String countryCodeString = String
        .format(view.getContext().getResources()
                .getString(R.string.phone_registration_spinner_item_country_code),
            item.telephoneCode);
    countryCode.setText(countryCodeString);

    TextView countryName = (TextView) view.findViewById(R.id.spinner_item_country);
    String countryNameString = String
        .format(view.getContext().getResources()
                .getString(R.string.phone_registration_spinner_item_country_name),
            item.countryName);
    countryName.setText(countryNameString);

    return view;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {

    view = inflater.inflate(R.layout.phonepicker_spinner_item, null);
    view.findViewById(R.id.spinner_item_flag).setVisibility(View.GONE);

    view.findViewById(R.id.spinner_item_country).setVisibility(View.GONE);

    PhonePickerItem item = items.get(i);
    TextView countryCode = (TextView) view.findViewById(R.id.spinner_item_country_code);
    String countryCodeString = String
        .format(view.getContext().getResources()
                .getString(R.string.phone_registration_spinner_item_country_code),
            item.telephoneCode);
    countryCode.setText(countryCodeString);
    return view;

  }

  public void setItems(List<PhonePickerItem> phonePickerItems) {
    items.addAll(phonePickerItems);
  }

  static class PhonePickerItem implements Comparable<PhonePickerItem> {

    private final int telephoneCode;
    private String countryName;
    private int flagResourceId;

    public PhonePickerItem(String countryName, int flagResourceId,
        int telephoneCode) {
      this.countryName = countryName;
      this.flagResourceId = flagResourceId;
      this.telephoneCode = telephoneCode;
    }

    @Override
    public int compareTo(PhonePickerItem o) {
      return this.countryName.toLowerCase().compareTo(o.countryName.toLowerCase());
    }

    public int getTelephoneCode() {
      return telephoneCode;
    }

    public String getCountryName() {
      return countryName;
    }

    public int getFlagResourceId() {
      return flagResourceId;
    }
  }
}
