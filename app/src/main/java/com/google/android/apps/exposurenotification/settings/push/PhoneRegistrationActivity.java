/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.push;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.apps.exposurenotification.R;
import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class PhoneRegistrationActivity extends AppCompatActivity {

  static final String TAG = PhoneRegistrationActivity.class.getSimpleName();
  public static final String PHONE_REGISTRATION_VERIFICATION_TOKEN = "PhoneRegistrationActivity.PHONE_REGISTRATION_VERIFICATION_TOKEN";
  public static final String PHONE_NUMBER = "PhoneRegistrationActivity.PHONE_NUMBER";
  public static final String TUTORIAL_FLOW = "PhoneRegistrationActivity.TUTORIAL_FLOW";
  private PhoneRegistrationViewModel phoneRegistrationViewModel;
  private boolean tutorialFlow;

  public static Intent newIntentForRegisterPhoneNumberFromTutorial(Context context) {
    Intent i = new Intent(context, PhoneRegistrationActivity.class);
    i.putExtra(TUTORIAL_FLOW, true);
    return i;
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_phone_registration);

    tutorialFlow = getIntent().getBooleanExtra(TUTORIAL_FLOW, false);

    // Set up events
    phoneRegistrationViewModel = new ViewModelProvider(this).get(PhoneRegistrationViewModel.class);

    phoneRegistrationViewModel.getRegistrationComplete().observe(
        this, wasCompleted -> {
          if (wasCompleted) {
            Toast.makeText(this, "Registration complete", Toast.LENGTH_LONG).show();
            phoneRegistrationViewModel.setShouldPromptForPhoneNumber(false);
            hideKeyboard();
            finish();
          }
        }
    );

    phoneRegistrationViewModel.subscribeTokenStoredEvent()
        .observe(this, wasTokenStored -> {
          if (!wasTokenStored) {
            return;
          }
          phoneRegistrationViewModel.registerFirebaseToken();
        });

    phoneRegistrationViewModel.subscribeVerificationToken()
        .observe(this, token -> {
          Bundle fragmentArguments = new Bundle();
          fragmentArguments.putString(PHONE_REGISTRATION_VERIFICATION_TOKEN, token);
          fragmentArguments.putString(PHONE_NUMBER,
              phoneRegistrationViewModel.getPhoneNumber().replace("+354", ""));
          Fragment fragment = new PhoneRegistrationVerificationFragment();
          fragment.setArguments(fragmentArguments);
          loadFragment(fragment);
          tutorialFlow = false;
        });

    Bundle b = new Bundle();
    b.putBoolean(TUTORIAL_FLOW, tutorialFlow);
    Fragment f = new PhoneNumberFragment();
    f.setArguments(b);
    loadFragment(f);

  }

  private void hideKeyboard() {
    InputMethodManager imm = (InputMethodManager) this
        .getSystemService(Activity.INPUT_METHOD_SERVICE);
    //Find the currently focused view, so we can grab the correct window token from it.
    View view = this.getCurrentFocus();
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
      view = new View(this);
    }
    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
  }

  private void loadFragment(Fragment fragment) {
    getSupportFragmentManager()
        .beginTransaction()
        .replace(
            R.id.register_phone_fragment,
            fragment)
        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        .commit();
  }

  @Override
  public void onBackPressed() {
    if (tutorialFlow) {
      return;
    }
    super.onBackPressed();
  }
}
