/*
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.google.android.apps.exposurenotification.settings.push;

import static com.google.android.apps.exposurenotification.settings.push.ForegroundMessageHandler.ON_PUSH_RECEIVED;
import static com.google.android.apps.exposurenotification.settings.push.ForegroundMessageHandler.TEST_RESULTS;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.apps.exposurenotification.R;
import com.google.android.apps.exposurenotification.network.AranjaUser;
import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class DisplayTestResultsDialogActivity extends AppCompatActivity {

  private boolean testResults;
  private PhoneRegistrationViewModel phoneregistrationViewModel;
  private AlertDialog dialog;


  public static Intent newIntent(AranjaUser aranjaUser, Context context) {
    Intent testResultsIntent = new Intent(context, DisplayTestResultsDialogActivity.class);
    testResultsIntent.putExtra(ON_PUSH_RECEIVED, true);
    testResultsIntent.putExtra(TEST_RESULTS, aranjaUser.getTestResult());
    testResultsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    return testResultsIntent;
  }


  // TODO handle positive / negative
  @Override
  protected void onResume() {
    super.onResume();
    if (!testResults) {
      loadDialog(getString(R.string.test_results_negative)).show();
    } else {
      loadDialog(getString(R.string.test_results_positive)).show();
    }
  }


  @Override
  protected void onPause() {
    super.onPause();
    if (dialog != null) {
      dialog.dismiss();
    }
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    phoneregistrationViewModel =
        new ViewModelProvider(this).get(PhoneRegistrationViewModel.class);
    if (getIntent().hasExtra(ON_PUSH_RECEIVED) && getIntent()
        .getBooleanExtra(ON_PUSH_RECEIVED, false)) {
      testResults = getIntent().getBooleanExtra(TEST_RESULTS, false);

    }
  }

  public Dialog loadDialog(String text) {
    View view = this.getLayoutInflater().inflate(R.layout.dialog_test_results, null);
    TextView textView = view.findViewById(R.id.border_test_results_test);
    textView.setText(text);
    if (this.dialog == null) {
      this.dialog = new AlertDialog.Builder(this)
          .setView(view)
          .setPositiveButton("Got it!", (dialog, id) -> {
            phoneregistrationViewModel.deleteTestResults();
            finish();
          }).create();
    }
    return dialog;
  }

}
