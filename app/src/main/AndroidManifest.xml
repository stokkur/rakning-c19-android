<?xml version="1.0" encoding="utf-8"?><!--
  ~ Copyright 2020 Google LLC
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      https://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~
  -->

<manifest xmlns:android="http://schemas.android.com/apk/res/android"
  xmlns:tools="http://schemas.android.com/tools"
  package="com.google.android.apps.exposurenotification">

  <application
    android:allowBackup="false"
    android:icon="@mipmap/ic_launcher"
    android:label="@string/app_name"
    android:name=".ExposureNotificationApplication"
    android:supportsRtl="true"
    android:theme="@style/ExposureNotification">

    <!-- Activities -->
    <activity
      android:clearTaskOnLaunch="true"
      android:exported="true"
      android:launchMode="singleTask"
      android:name=".home.ExposureNotificationActivity"
      android:windowSoftInputMode="stateHidden">
      <intent-filter>
        <action android:name="android.intent.action.MAIN" />
        <category android:name="android.intent.category.LAUNCHER" />
      </intent-filter>
    </activity>

    <activity
      android:exported="false"
      android:label="@string/exposure_about_title"
      android:name=".settings.ExposureAboutActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:windowSoftInputMode="stateHidden" />

    <activity
      android:exported="true"
      android:name=".notify.ShareDiagnosisActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:windowSoftInputMode="stateHidden|adjustResize">
      <intent-filter>
        <action android:name="android.intent.action.VIEW" />

        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />

        <data android:scheme="ens" />
      </intent-filter>
      <intent-filter android:autoVerify="true">
        <action android:name="android.intent.action.VIEW" />

        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />

        <data
          android:host="${appLinkHost}"
          android:scheme="https" />
      </intent-filter>
    </activity>

    <activity
      android:exported="false"
      android:label="@string/app_analytics_title"
      android:name=".settings.AppAnalyticsActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:windowSoftInputMode="stateHidden|adjustResize" />

    <activity
      android:exported="false"
      android:label="@string/app_analytics_title"
      android:name=".settings.push.PhoneRegistrationActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:windowSoftInputMode="stateHidden|adjustResize" />

    <activity
      android:exported="false"
      android:label="@string/agency_message_title"
      android:name=".settings.AgencyActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:windowSoftInputMode="stateHidden|adjustResize" />

    <activity
      android:exported="false"
      android:label="@string/settings_legal_terms"
      android:name=".settings.LegalTermsActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:windowSoftInputMode="stateHidden|adjustResize" />

    <activity
      android:exported="false"
      android:label="@string/landlaeknir_user_tutorial"
      android:launchMode="singleTask"
      android:name=".tutorial.TutorialActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:screenOrientation="portrait"
      android:windowSoftInputMode="stateHidden|adjustResize" />

    <activity
      android:exported="false"
      android:label="@string/display_test_results"
      android:launchMode="singleTask"
      android:name=".settings.push.DisplayTestResultsDialogActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:screenOrientation="portrait"
      android:windowSoftInputMode="stateHidden|adjustResize" />


    <activity
      android:exported="false"
      android:label="@string/language_selection"
      android:launchMode="singleTask"
      android:name=".settings.language.LanguageSelectActivity"
      android:parentActivityName=".home.ExposureNotificationActivity"
      android:screenOrientation="portrait"
      android:windowSoftInputMode="stateHidden|adjustResize" />


    <!-- Receivers -->
    <meta-data
      android:name="preloaded_fonts"
      android:resource="@array/preloaded_fonts" />

    <provider
      android:authorities="${applicationId}.workmanager-init"
      android:name="androidx.work.impl.WorkManagerInitializer"
      tools:node="remove" />

    <receiver
      android:exported="true"
      android:name=".nearby.ExposureNotificationBroadcastReceiver"
      android:permission="com.google.android.gms.nearby.exposurenotification.EXPOSURE_CALLBACK">
      <intent-filter>
        <action android:name="com.google.android.gms.exposurenotification.ACTION_EXPOSURE_STATE_UPDATED" />
        <action android:name="com.google.android.gms.exposurenotification.ACTION_EXPOSURE_NOT_FOUND" />
      </intent-filter>
    </receiver>

    <service
      android:directBootAware="true"
      android:exported="false"
      android:name=".settings.push.ForegroundMessageHandler">
      <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT" />
      </intent-filter>
    </service>

  </application>

  <uses-feature android:name="android.hardware.bluetooth" />

  <uses-feature
    android:name="android.hardware.bluetooth_le"
    android:required="true" />

  <uses-permission android:name="android.permission.BLUETOOTH" />

  <uses-permission android:name="android.permission.INTERNET" />

</manifest>
